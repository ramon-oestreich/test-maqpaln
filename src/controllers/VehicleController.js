const Vehicle = require('../models/Vehicle');

class VehicleController {

  async store(req, res) {

    const vehicle = new Vehicle(req.body);
    await vehicle
        .save()
        .then(response => {
          return res.status(200).json(response);
        })
        .catch(err => {
          return res.status(500).json(err);
        });
  }

  async update(req, res) {

    await Vehicle.findOneAndUpdate({ 'placa': req.params.placa },
        req.body, { new: true })
        .then(response => {
          if (response == null) {
            return res.status(404).json({
              message: `Placa ${req.params.placa} não encontrada!`
            });
          }
          return res.status(200).json(response);
         })
        .catch(err => {
            return res.status(500).json(err);
        });
  }

  async destroy(req, res) {

    await Vehicle.findOneAndDelete({ 'placa': req.params.placa })
        .then(response => {
          if (response == null) {
            return res.status(404).json({
              message: `Placa ${req.params.placa} não encontrada!`
            });
          }
          return res.status(200).json(response);
        })
        .catch(err => {
          return res.status(500).json(err);
        });
  }

  async show(req, res) {

    await Vehicle.findOne({ 'placa': req.params.placa })
        .then(response => {
          if (response == null) {
            return res.status(404).json({
              message: `Placa ${req.params.placa} não encotrada!`
            });
          }
          return res.status(200).json(response);
        })
        .catch(err => {
          return res.status(500).json(err);
        });
  }

  async totalBrand(req, res) {

    await Vehicle.aggregate([{
      $match: {
        marca: req.params.marca
      }
    },
    {
      $project: {
        total: {
          $sum: '$revisoes.valor'
        }
      }
    }
    ]).then(response => {
      if (Object.keys(vehicle).length == 0) {
        return res.status(404).json({
          message: `Nenhum valor encontrado!`
        });
      }
      return res.status(200).json(response);
    }).catch(err => {
      return res.status(500).json(err);
    });
  }

  async totalBoard(req, res) {

    await Vehicle.aggregate([{
      $match: {
        placa: req.params.placa,
      }
    },
    {
      $project: {
        total: {
          $sum: "$revisoes.valor",
        }
      }
    },
    ]).then(response => {
      if (Object.keys(vehicle).length == 0) {
        return res.status(404).json({
          message: `Nenhum valor encontrado!`
        });
      }
      return res.status(200).json(response);
    }).catch(err => {
      return res.status(500).json(err);
    });
  }

  async filter(req, res) {

    const { marca, cor } = req.params;

    await Vehicle.find({ marca, cor })
        .then(response => {
          if (Object.keys(response).length === 0) {
            return res.status(404).json({
              message: `Nenhum Veiculo encontrado!`
            });
          }
          return res.status(200).json(response);
        })
        .catch(err => {
          return res.status(500).json(err);
        });
  }

  async addReview(req, res) {

    const { revisoes } = req.body;

    await Vehicle.findOneAndUpdate({ 'placa': req.params.placa },
        { $push: { revisoes }
        }, { new: true })
        .then(response => {
          if (response == null) {
            return res.status(404).json({
              message: `Veiculo não encontrado!`
            });
          }
          return res.status(200).json(response);
        })
        .catch(err => {
          return res.status(500).json(err);
        });
  }

}
module.exports = new VehicleController();