const mongoose = require('../config/database');
const Schema = mongoose.Schema;

const VehicleSchema = new Schema({
  placa: {
    type: String,
    required: true,
    unique: true,
  },
  marca: {
    type: String,
    required: true,
  },
  cor:{
      type:String,
      required:true
  },
  modelo: {
    type: String,
    required: true,
  },
  ano_fabricacao: {
    type: Date,
    required: true,
  },
  data_cadastro: {
    type: Date,
    default: Date.now(),
    required: true,
  },
  revisoes: [
    {
      data_revisao: {
        type: Date,
        required: true,
      },
      valor: {
        type: Number,
        required: true,
      },
    },
  ],
});

module.exports = Vehicle = mongoose.model('Vehicle',VehicleSchema);
