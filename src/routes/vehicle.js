const express = require('express');
const VehicleController =  require('../controllers/VehicleController');
const router = express.Router();

router.post('/', VehicleController.store);
router.get('/:placa', VehicleController.show);
router.get('/total/marca/:marca', VehicleController.totalBrand);
router.get('/total/placa/:placa', VehicleController.totalBoard);
router.get('/filtro/:marca/:cor',VehicleController.filter)
router.put('/:placa', VehicleController.update);
router.delete('/:placa', VehicleController.destroy);
router.patch('/:placa/revisao', VehicleController.addReview);

module.exports = router;