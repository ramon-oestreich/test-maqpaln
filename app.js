const express =  require('express');
const app =  express();
const PORT = 3000;

const bodyParser = require("body-parser");
const vehicleRouter = require('./src/routes/vehicle');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api/veiculos/',vehicleRouter);

app.listen(PORT,()=>{
    console.log(`running in port ${PORT}`);
})